from channels import Group


def ws_message(message):
    print('saving message..')
    Group('live').send({
        'text': message.content['text'],
    })


def ws_connect(message):
    # Accept the incoming connection
    message.reply_channel.send({"accept": True})
    Group("live").add(message.reply_channel)


def ws_disconnect(message):
    Group("live").discard(message.reply_channel)
